# The Freenode resignation FAQ, or: "what the fuck is going on?"

## IMPORTANT NOTE:

It's come to my attention that some people have been spamming issue trackers with a link to this gist. While it's a good idea to inform people of the situation in principle, please __do not do this__. By all means spread the word in the communities that you are a part of, *after* verifying that they are not aware yet, but unsolicited spam is not helpful. It will just frustrate people.

## Update 3 (May 24, 2021)

A number of things have happened since the last update.

- Andrew [broke Freenode policies to steal a channel](https://www.devever.net/~hl/freenode_abuse)
- ... and then unilaterally [changed the Freenode policies afterwards](https://github.com/freenode/web-7.0/pull/513/files) to try and retroactively justify it - later adding a note claiming it's a draft, but the PR has already been merged.
- That change also removed the rule against hateful speech on the network, at the same time.
- The channel #freenode-policy-feedback was indicated as the venue for discussion and feedback around the policy changes. After a number of inconvenient (and as of yet unanswered) questions from various users, Andrew sent the people in #freenode to the channel, quickly turning it into an argumentative mess full of trolling, and drowning the (still unanswered) questions.

Andrew was repeatedly asked in the "policy discussion channel" whether slurs, racism and transphobia are now permitted on Freenode. No answer has been forthcoming. This is the clearest answer that has been provided at the time of writing:

> [21:18:02 CEST] &lt;joepie91&gt;	<G-Flex> rasengan: If you need an example: Will racial slurs be tolerated within network policy? What about racism in general, or transphobia?  
  [21:22:24 CEST] &lt;sellow&gt;	rasengan: Is racism, for example referring to groups of people using a derogatory term, now permitted under the network policy?  
  [21:22:57 CEST] &lt;rasengan&gt;	sellow: This is a great question that absolutely requires community feedback.  I can't answer to it because the former freenode staff used derogatory terms so its still unclear to me, I'm still trying to understand the policy.  Again, community feedback is appreciated.  
  [21:23:06 CEST] &lt;rasengan&gt;	Please stop asking questions as it relates to this topic as that is the answer.  
  [21:23:18 CEST] &lt;rasengan&gt;	(Community feedback is required)  
  [21:24:14 CEST] &lt;joepie91&gt;	rasengan: am I understanding correctly that you are leaving "whether slurs, racism and transphobia are allowed on Freenode" to "community feedback"?  
  [21:26:23 CEST] &lt;rasengan&gt;	joepie91/ave_: I'm not really, again, even clear how that's defined and handled as per current standing freenode policy, draft or not, given the previous staff engagement in said activity.  We're asking for community feedback, as 'rules' that the community must follow should be determined with the community, not without.

Chatter from other users has been snipped from the above excerpt for readability. As usual, the [full logs with full context](https://paste.sr.ht/~ircwright/0c973eb56d411f1c0e04a41790cd99353d27bc75) are available for your perusal.

Not a clear answer by any stretch of the imagination, but I think everybody knows what such a non-answer *really* means when someone has been actively avoiding the question for over an hour.

After someone asked what the purpose of the channel was since no questions seemed to get answered, and I pointed out that it seemed like a sinkhole for criticism, [both I and the question asker(!) were banned by Andrew](https://paste.sr.ht/~ircwright/0c973eb56d411f1c0e04a41790cd99353d27bc75#-L2509).

## Update 2

__Freenode staff have stepped down. The network that runs at freenode.org/net/com should now be assumed to be under control of a malicious party.__ Andrew Lee will likely gain control over the NickServ database at some point.

I recommend that you move to [libera.chat](https://libera.chat/) as soon as possible, as the former Freenode staff has left the network that exists on the Freenode domains, and so they can no longer guarantee the safety of your information or community there. Basically, libera.chat is the continuation of Freenode.

- kline: https://www.kline.sh/
- jess: https://gist.github.com/jesopo/45a3e9cdbe517dc55e6058eb43b00ed9
- Md: https://blog.bofh.it/debian/id_461
- niko: https://coevoet.fr/freenode.html
- edk: https://gist.github.com/edk0/478fb4351bc3ba458288d6878032669d
- emilsp: https://gist.github.com/pinkisemils/39d4ded8b1639b6f39dcab15618649f5
- mniip: https://mniip.com/freenode.txt
- Swant: https://swantzter.se/freenode-resignation/
- JonathanD: https://gist.github.com/JonathanD82/6518b93708e0aaf4b8f92c8e7200816d

(A special note for marginalized communities: I won't go into the details further in this gist, but I personally believe that Freenode will not be a safe place for you going forward.)

## Update 1

Since this gist was originally written, things have taken a turn for the worse, and two Freenode staffers have stepped down. Their resignation letters describe the situation in quite a bit of detail:

- Fuchs: https://fuchsnet.ch/freenode-resign-letter.txt
- amdj: https://gist.github.com/aaronmdjones/1a9a93ded5b7d162c3f58bdd66b8f491

What it appears to come down to, is that Andrew Lee is attempting to essentially take over the network by drowning staff in lawyers.

It also turns out that my understanding of Andrew being "locked out" from the domain may have been wrong, and in fact they only recently gained access that they previously didn't have. This is all still a bit unclear, though.

Original post below.

## Disclaimer

Hi! I wrote this FAQ to give people a bit more insight into what's going on with Freenode, Andrew Lee, and the leaked resignation letters.

I want to be very clear about this: __I am not a Freenode staffer, and this document only contains information from public channels__. There are no scoops here, and while I believe that my understanding of the situation is accurate, it's always possible that it's not. Much of this information is correlated from statements made and sources provided by other people.

I have not sought permission from Freenode staff to write this FAQ, nor have I told them that I would do so. They've not verified any of the information in here, nor have I asked them to. In short: they are not involved in this FAQ whatsoever.

Also, my personal view on the situation is that Andrew Lee is decidedly in the wrong here, and has no legitimate claim to Freenode (the IRC network and voluntary association). I believe that his intentions are malicious, and that he should under no circumstances gain control over the network, nor access to its user data. Just so that you know where I'm coming from.

Now, with that out of the way...

## OK, so what's going on?

You're probably here because you've read a resignation letter from a Freenode staffer, indicating that they are stepping down. You might've asked around about the reasons, and heard that actually, they *haven't* stepped down, it was just a leaked draft. WTF?

So, let's start: Andrew Lee is, indirectly, the owner of a company named "Freenode Limited". Despite what the name implies, this company actually has nothing to do with the operation of the Freenode IRC network, really; it was apparently established to handle Freenode Live finances. However, through a number of vague historical events, the company also gained control over the Freenode domain names.

Importantly, this company has absolutely no operational involvement. It does not own any servers, it does not employ any of the staff. It does not sponsor any infrastructure. All the staff are unpaid volunteers, and all the servers are sponsored by third parties - on a rolling basis. No contracts exist between either Freenode Limited and the staff, or between Freenode Limited and the sponsors. Literally the only piece of infrastructure that Freenode Limited (claims to) own, are the domain names. No money flows from Freenode Limited to the operational side of the network.

Now, it seems that Andrew Lee - despite zero involvement in the day-to-day operations of Freenode - has decided that ownership of the domains entitles him to ownership of Freenode as a network and community, and intended to give his own people administrative access to the network, without involving the staff team in this decision.

The details are vague at this point, but it's safe to say that the staff disagreed - ~~Andrew was apparently locked out of the domains~~, and the staff team collectively drafted resignation letters, to be published in the event that Andrew would misuse his domain ownership to do a hostile takeover of the network.

Crucially, stepping down and moving to another network would be a last resort, and staff had no intention of stepping down unless there was no other option. Through an error, one of these letters - linking to the others - was found by Andrew Lee and others, before it was intended to be published.

~~Andrew has not yet regained access to the domains.~~ ~~At the time of writing, none of the staff have made the decision to step down.~~ It is, however, still very much up in the air.

Freenode Limited has failed to file its company filings in time, and is scheduled to be struck from the company register. Andrew appears to be under the strange impression that it is the volunteer staff's responsibility to do the filings, despite them having no contract nor affiliation with the company whatsoever, and receiving no payment. I am unaware of the details, but some sort of legal threats are also flying around.

If you have some free time, [these logs](https://paste.sr.ht/blob/4f5c7a6b3f6adb4697572f8cd77582fa16a123de) contain a *lot* of information about the situation. In those logs, `rasengan` is Andrew Lee. There's also [these logs](https://paste.sr.ht/~ircwright/7e751d2162e4eb27cba25f6f8893c1f38930f7c4) preceding them, from the main #freenode channel - they provide a bit more insight into the start of the incident.

## What does this mean for my channel or community on Freenode?

__Freenode staff have stepped down. The network that runs at freenode.org/net/com should be assumed to be under control of a malicious party.__ I'd recommend moving your community to [libera.chat](https://libera.chat/) as soon as possible.

~~Right now: nothing. Andrew does not currently have control over the domain, nor do he or any of his associates have access to any of the Freenode infrastructure. The infrastructure is, at the time of writing, fully controlled by the staff team that you've come to know and maybe even love.~~

~~This may change. If Andrew were to regain domain access and attempt a hostile takeover of the network through that access, the Freenode staff will in all likelihood step down.~~ Since the relations of server sponsors are with the staff team, not with Andrew Lee, it is very likely that the entire network infrastructure will come along with wherever the staff decides to go.

In other words: in this event, Freenode as a network will cease to exist. The domain name will point at either nothing, or at completely different infrastructure run by completely different people (on behalf of Andrew Lee), which shares nothing with the Freenode of old other than the name. The old infrastructure run by the staff team may reappear under a new name, or be decommissioned. 

However, Andrew has not exactly made himself popular with the community (see the logs linked above). He's also made it quite clear that his intentions are *not* to act in the best interest of the community, and he has failed to answer many crucial questions, including questions about the dubious finances of Freenode Limited. It's possible that he realizes that he has nothing to win here, and decides to hand over the domains to the current staff. In this case, nothing will change at all.

Realistically, the best approach for you as a community leader right now is to remain calm, and closely watch for updates from the staff. If action is required to eg. move your community, you will likely be made aware of this. It's probably a good idea to start thinking about contingency plans for the worst-case scenario, though.
